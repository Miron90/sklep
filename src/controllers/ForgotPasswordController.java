package controllers;

import helpers.Changer;
import helpers.Validation;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.ResourceBundle;

public class ForgotPasswordController implements Initializable {

    @FXML
    Button backBt,changePasswordBt,signUpBt;
    @FXML
    ImageView quit,panel;
    @FXML
    Label error;
    @FXML
    TextField loginField;
    @FXML
    PasswordField passwordField,repeatPasswordField;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        backBt.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Changer.changeScene(event,"../fxml/login.fxml");
            }
        });
        signUpBt.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Changer.changeScene(event,"../fxml/register.fxml");
            }
        });
        changePasswordBt.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (Validation.validateForgotPasswrod(loginField,passwordField,repeatPasswordField,error)){
                    error.setVisible(false);
                    Changer.changeScene(event,"../fxml/login.fxml");
                }
                else{
                    error.setText("Nie prawidłowe dane");
                    error.setVisible(true);
                }

            }
        });
        panel.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                Changer.getStage().setIconified(true);
            }
        });
        quit.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                Platform.exit();
            }
        });

    }
}
