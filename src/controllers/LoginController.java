package controllers;

import helpers.Changer;
import helpers.Validation;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import sample.Main;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;


public class LoginController implements Initializable {
    @FXML
    Button forgotPasswordBt,signUpBt,signInBt;
    @FXML
    PasswordField passwordField;
    @FXML
    TextField loginField;
    @FXML
    Label error;
    @FXML
    ImageView quit,panel;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        error.setVisible(false);
        signInBt.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                    if(Validation.validateLoginData(loginField,passwordField,error)){
                        Changer.changeScene(event,"../fxml/shop.fxml");
                    }
                    else{
                        error.setVisible(true);
                    }
            }
        });
        forgotPasswordBt.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                    Changer.changeScene(event,"../fxml/forgot_password.fxml");
            }
        });
        signUpBt.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Changer.changeScene(event,"../fxml/register.fxml");
            }
        });
        panel.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                if( Changer.getStage()==null){
                    Main.getStage().setIconified(true);
                }else {
                    Changer.getStage().setIconified(true);
                }
            }
        });
        quit.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                Platform.exit();
            }
        });
    }
}
