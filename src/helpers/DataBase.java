package helpers;

import javafx.scene.control.Label;
import sample.Account;

import java.sql.*;

public class DataBase {
    public Connection connect;

    public boolean zaloguj(String login, String haslo) throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
        getConnection();
        String SQLquery="SELECT * FROM konta WHERE Login=\'"+login+"\' AND Haslo=\'"+haslo+"\' LIMIT 1";
        Statement statement=connect.createStatement();
        ResultSet rs=statement.executeQuery(SQLquery);
        if(rs.next()) {
            Account account=new Account(rs.getString(1),rs.getInt(4),rs.getInt(3));
            connect.close();
            Validation.setKonto(account);
            return true;
        }
        else {
            connect.close();
            return false;
        }
    }

    public Connection getConnection() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException{
        Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
        connect= DriverManager.getConnection("jdbc:mysql://localhost:3306/programowaniezdarzeniowe?useTimezone=true&serverTimezone=UTC","root","");
        return connect;
    }

    public boolean zarejestruj(String login, String haslo, int age, Label error) throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
            getConnection();
            String SQLquery1="SELECT * FROM konta WHERE login="+login+"LIMIT 1";
            Statement statement1=connect.createStatement();
            ResultSet rs = statement1.executeQuery(SQLquery1);
            if(rs.next()){
                error.setText("Podany login jest już zajęty");
                error.setVisible(true);
                return false;
            }
            String SQLquery2="INSERT INTO konta VALUES ('"+login+"','"+haslo+"','"+age+"','"+0+"')";
            Statement statement=connect.createStatement();
            statement.executeUpdate(SQLquery2);
            connect.close();
        return true;
    }

    public boolean przypomnijHaslo(String login,String noweHaslo) throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
            getConnection();
        String SQLquery1="SELECT * FROM konta WHERE Login=\'"+login+"\' LIMIT 1";
        Statement statement1=connect.createStatement();
        ResultSet rs=statement1.executeQuery(SQLquery1);
        if(rs.next()) {
            String SQLquery2="UPDATE konta SET Haslo='"+noweHaslo+"'WHERE Login='"+login+"'";
            Statement statement2=connect.createStatement();
            statement2.executeUpdate(SQLquery2);
            connect.close();
            return true;
        }
        else {
            connect.close();
            return false;
        }
    }
}
