package helpers;

import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import sample.Account;

import java.sql.SQLException;

public class Validation {
    private static Account konto=null;

    public static boolean validateLoginData(TextField login, PasswordField password, Label error) {
        /*final Thread thread = new Thread(task, "task-thread");
        thread.setDaemon(true);
        thread.start();*/
        DataBase db=new DataBase();
        if(login.getText().matches("^(?=.*[a-z])(?=.*[A-Z])[a-zA-Z\\d]{5,20}$")&&password.getText().matches("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{5,20}$")){
            try {
                if(db.zaloguj(login.getText(),password.getText())){
                    return true;
                }else{
                    error.setText("Nie prawidłowe dane logowania");
                    return false;
                }
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        else{
            error.setText("Nie prawidłowe dane logowania");
            return false;
        }
        error.setText("Nie można połączyć się z bazą danych");
        return false;
    }
    public static boolean validateRegister(TextField login,PasswordField password,PasswordField repeatPassword,TextField age,Label error){
        DataBase db=new DataBase();
        try {
            if (login.getText().matches("^(?=.*[a-z])(?=.*[A-Z])[a-zA-Z\\d]{5,20}$") && password.getText().matches("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{5,20}$") && repeatPassword.getText().matches("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{5,20}$") && password.getText().equals(repeatPassword.getText()) && Integer.parseInt(age.getText()) > 0) {
                try {
                    return db.zarejestruj(login.getText(), password.getText(), Integer.parseInt(age.getText()), error);
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                } catch (SQLException e) {
                    e.printStackTrace();
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            } else {
                error.setText("Nie prawidłowe dane");
                return false;
            }
            error.setText("Nie można połączyć się z bazą danych");
            return false;
        }
        catch(NumberFormatException e){
            e.printStackTrace();
            return false;
        }
    }
    public static boolean validateForgotPasswrod(TextField login,PasswordField password,PasswordField repeatPassword,Label error){
        DataBase db=new DataBase();
        try {
            if (login.getText().matches("^(?=.*[a-z])(?=.*[A-Z])[a-zA-Z\\d]{5,20}$") && password.getText().matches("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{5,20}$") && repeatPassword.getText().matches("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{5,20}$") && password.getText().equals(repeatPassword.getText())) {
                try {
                    return db.przypomnijHaslo(login.getText(), password.getText());
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                } catch (SQLException e) {
                    e.printStackTrace();
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            } else {
                error.setText("Nie prawidłowe dane");
                return false;
            }
            error.setText("Nie można połączyć się z bazą danych");
            return false;
        }
        catch(NumberFormatException e){
            e.printStackTrace();
            return false;
        }
    }
    public static void setKonto(Account k){
        konto=k;
    }
     /*static final Task<Void> task = new Task<Void>() {
        @Override
        protected Void call() throws Exception {

            displayResponse("Testing Complete!");

            return null;
        }
        private void displayResponse(String testResultMessage){
            Platform.runLater(()->{
                DataBase.Login();
            });
        }
    };*/
}

